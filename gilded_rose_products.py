

class GenericProduct(object):
    def __init__(self, item):
        self.item = item

    def progress(self):
        self.update_quality()
        self.update_sell_in()

    def update_quality(self):
        """Reduce quality if it is above 0. Quality is reduced by 2 if sell_in is below 0"""
        if self.item.quality > 0:
            if self.item.sell_in <= 0:
                self.item.quality -= 2
            else:
                self.item.quality -= 1

    def update_sell_in(self):
        """Sell in reduces every day"""
        self.item.sell_in -= 1


class AgedBrie(GenericProduct):
    def update_quality(self):
        """Increase quality if quality below 50. If sell_in is below 0 quality increases by 2"""
        if self.item.quality < 50:
            if self.item.sell_in <= 0:
                self.item.quality += 2
            else:
                self.item.quality += 1


class Sulfuras(GenericProduct):
    def update_quality(self):
        """Overwrite this method with no operation"""
        pass

    def update_sell_in(self):
        """Overwrite this method with no operation"""
        pass


class BackStagePass(GenericProduct):
    def update_quality(self):
        """Quality increases by 1, 2 or 3 depending on sell_in. If sell_in is below 0 then quality is 0."""
        if self.item.sell_in > 10:
            self.item.quality += 1
        if 5 < self.item.sell_in <= 10:
            self.item.quality += 2
        elif 0 < self.item.sell_in <= 5:
            self.item.quality += 3
        elif self.item.sell_in <= 0:
            self.item.quality = 0


class Conjured(GenericProduct):
    def update_quality(self):
        """Quality decreases twice as fast as GenericProduct"""
        if self.item.quality > 0:
            if self.item.sell_in <= 0:
                self.item.quality -= 4
            else:
                self.item.quality -= 2


PRODUCT_NAME_TO_CLASS = {'Aged Brie': AgedBrie,
                         'Sulfuras, Hand of Ragnaros': Sulfuras,
                         'Backstage passes to a TAFKAL80ETC concert': BackStagePass,
                         'Conjured': Conjured}

DEFAULT_PRODUCT_CLASS = GenericProduct
