from gilded_rose import Item
from gilded_rose_products import GenericProduct, AgedBrie, Sulfuras, BackStagePass, Conjured
import mock
import unittest


class GenericProductTests(unittest.TestCase):

    def test_update_quality(self):
        item = Item('product', 3, 5)
        GenericProduct(item).update_quality()
        self.assertEqual(4, item.quality)

        item = Item('product', 3, 0)
        GenericProduct(item).update_quality()
        self.assertEqual(0, item.quality)

        item = Item('product', 0, 5)
        GenericProduct(item).update_quality()
        self.assertEqual(3, item.quality)

    def test_update_sell_in(self):
        item = Item('product', 3, 5)
        GenericProduct(item).update_sell_in()
        self.assertEqual(2, item.sell_in)

    @mock.patch('gilded_rose_products.GenericProduct.update_quality')
    @mock.patch('gilded_rose_products.GenericProduct.update_sell_in')
    def test_progress(self, update_quality, update_sell_in):
        product = GenericProduct(None)
        product.progress()
        self.assertTrue(update_quality.called)
        self.assertTrue(update_sell_in.called)


class AgedBrieTests(unittest.TestCase):
    def test_update_quality(self):
        item = Item('product', 3, 5)
        AgedBrie(item).update_quality()
        self.assertEqual(6, item.quality)

        item = Item('product', 3, 50)
        AgedBrie(item).update_quality()
        self.assertEqual(50, item.quality)

        item = Item('product', 0, 5)
        AgedBrie(item).update_quality()
        self.assertEqual(7, item.quality)


class SulfurasTests(unittest.TestCase):
    def test_update_quality(self):
        item = Item('product', 3, 5)
        Sulfuras(item).update_quality()
        self.assertEqual(5, item.quality)

    def test_update_sell_in(self):
        item = Item('product', 3, 5)
        Sulfuras(item).update_sell_in()
        self.assertEqual(3, item.sell_in)


class BackStagePassTests(unittest.TestCase):
    def test_update_quality(self):
        item = Item('product', 15, 5)
        BackStagePass(item).update_quality()
        self.assertEqual(6, item.quality)

        item = Item('product', 10, 5)
        BackStagePass(item).update_quality()
        self.assertEqual(7, item.quality)

        item = Item('product', 5, 5)
        BackStagePass(item).update_quality()
        self.assertEqual(8, item.quality)

        item = Item('product', 0, 5)
        BackStagePass(item).update_quality()
        self.assertEqual(0, item.quality)


class ConjuredTests(unittest.TestCase):
    def test_update_quality(self):
        item = Item('product', 3, 5)
        Conjured(item).update_quality()
        self.assertEqual(3, item.quality)

        item = Item('product', 0, 5)
        Conjured(item).update_quality()
        self.assertEqual(1, item.quality)


if __name__ == '__main__':
    unittest.main()
