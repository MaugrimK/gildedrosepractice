import unittest

from gilded_rose import GildedRose, Item


class BaselineTest(unittest.TestCase):
    items = [Item("Random item", 5, 5),
             Item("Random item", 0, 5),
             Item("Random item", 0, 0),
             Item("Aged Brie", 5, 5),
             Item("Aged Brie", 0, 0),
             Item("Aged Brie", 0, 50),
             Item("Sulfuras, Hand of Ragnaros", 0, 80),
             Item("Backstage passes to a TAFKAL80ETC concert", 20, 20),
             Item("Backstage passes to a TAFKAL80ETC concert", 10, 20),
             Item("Backstage passes to a TAFKAL80ETC concert", 5, 20),
             Item("Backstage passes to a TAFKAL80ETC concert", 0, 20),
             ]

    def test_baseline(self):
        expected_result = ['***Day0***',
                           'Random item, 5, 5',
                           'Random item, 0, 5',
                           'Random item, 0, 0',
                           'Aged Brie, 5, 5',
                           'Aged Brie, 0, 0',
                           'Aged Brie, 0, 50',
                           'Sulfuras, Hand of Ragnaros, 0, 80',
                           'Backstage passes to a TAFKAL80ETC concert, 20, 20',
                           'Backstage passes to a TAFKAL80ETC concert, 10, 20',
                           'Backstage passes to a TAFKAL80ETC concert, 5, 20',
                           'Backstage passes to a TAFKAL80ETC concert, 0, 20',
                           '***Day1***',
                           'Random item, 4, 4',
                           'Random item, -1, 3',
                           'Random item, -1, 0',
                           'Aged Brie, 4, 6',
                           'Aged Brie, -1, 2',
                           'Aged Brie, -1, 50',
                           'Sulfuras, Hand of Ragnaros, 0, 80',
                           'Backstage passes to a TAFKAL80ETC concert, 19, 21',
                           'Backstage passes to a TAFKAL80ETC concert, 9, 22',
                           'Backstage passes to a TAFKAL80ETC concert, 4, 23',
                           'Backstage passes to a TAFKAL80ETC concert, -1, 0',
                           ]

        result = ['***Day0***']
        result.extend([str(item) for item in self.items])
        GildedRose(self.items).update_quality()
        result.append('***Day1***')
        result.extend([str(item) for item in self.items])

        self.assertEqual(expected_result, result)


if __name__ == '__main__':
    unittest.main()
