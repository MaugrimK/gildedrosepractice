# -*- coding: utf-8 -*-
import unittest
import mock
from gilded_rose import Item, GildedRose


class TestClass1(object):
    def __init__(self, *args, **kwargs):
        pass

    def progress(self):
        pass


class TestClass2(TestClass1):
        pass


class GildedRoseTest(unittest.TestCase):
    def test_next_day(self):
        items = [Item("foo", 0, 0)]
        gilded_rose = GildedRose(items)
        gilded_rose.update_quality()
        self.assertEquals("foo", gilded_rose.items[0].name)
        self.assertEquals(-1, gilded_rose.items[0].sell_in)
        self.assertEquals(0, gilded_rose.items[0].quality)

    @mock.patch.object(TestClass1, 'progress')
    @mock.patch.object(TestClass2, 'progress')
    def test_process_items(self, progress1, progress2):
        """Test that progress method was called on all necessary classes"""
        items = [Item("Product1", 5, 5), Item("Product2", 3, 3)]
        gilded_rose = GildedRose(items)
        gilded_rose.items_to_classes = {'Product1': TestClass1}
        gilded_rose.default_class = TestClass2
        gilded_rose.update_quality()
        self.assertTrue(progress1.called, progress2.called)


if __name__ == '__main__':
    unittest.main()
